package com.cerotid.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.cerotid"})
public class RestfulMessengerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulMessengerApiApplication.class, args);
	}
}
