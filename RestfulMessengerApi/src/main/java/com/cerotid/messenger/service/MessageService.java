package com.cerotid.messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cerotid.messenger.database.DatabaseClass;
import com.cerotid.messenger.model.Message;

public class MessageService {

	private Map<Long, Message> messages = DatabaseClass.getMessages();
	
	public MessageService() {
		messages.put(1L, new Message(1,"Hello Japan","Xung Lee"));
		messages.put(2L, new Message(2,"Hello china","Leee Xung"));
		messages.put(3L, new Message(3,"Hello India","Salman Khan"));
	}

	public List<Message> getAllMessage() {
		return new ArrayList<Message>(messages.values());
	}
	
	public Message getMessage(long id) {
		return messages.get(id);		
	}
	
	public Message addMessage(Message message) {
		message.setId(messages.size() +1);
		messages.put(message.getId(),message);
		return message;
				
	}

	public Message updateMessage(Message message) {
		if (message.getId() <= 0) {
			return null;
		}
		messages.put(message.getId(),message);
		return message;
	}
	
	public Message removeMessage(long id) {
		return messages.remove(id);
	}
}
