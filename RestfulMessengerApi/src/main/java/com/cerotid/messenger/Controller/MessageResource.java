package com.cerotid.messenger.Controller;

import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cerotid.messenger.model.Message;
import com.cerotid.messenger.service.MessageService;

@RestController
@RequestMapping(path = "/messages", produces = MediaType.APPLICATION_JSON)
public class MessageResource {

	MessageService messageService = new MessageService();

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public List<Message> getMessages() {
		return messageService.getAllMessage();
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{messageId}", produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public Message getMessage(@PathVariable long messageId) {
		return messageService.getMessage(messageId);
	}
	
	@RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public Message addMessage(@RequestBody Message message) {
		return messageService.addMessage(message);
	}
	
}
